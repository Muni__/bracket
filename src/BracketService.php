<?php


namespace Esenov;

use Symfony\Component\Console\Output\OutputInterface;

class BracketService
{
    /**
     * Service for checking $string
     *
     * @param string $string
     * @param OutputInterface $output
     *
     * @return bool
     */
    public static function check(string $string, OutputInterface $output): bool {
        $bracketChecker = new Bracket();

        try {
            return $bracketChecker->check($string);
        }catch (\Exception $e){
            $output->writeln("<error>{$e->getMessage()}</error>");
            return false;
        }
    }
}