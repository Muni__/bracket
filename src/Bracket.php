<?php
namespace Esenov;

use Esenov\Exceptions\EmptyStringException;
use InvalidArgumentException;
use phpDocumentor\Reflection\Types\Boolean;

class Bracket
{
    /**
     * @var int $pool
     */
    private $pool;

    public function __construct()
    {
        $this->pool = 0;
    }

    /**
     * Check open-close brackets
     *
     * @param string $string
     *
     * @return bool
     *
     * @throws EmptyStringException
     * @throws InvalidArgumentException
     */
    public function check(string $string): bool
    {
        if ($string == "") {
            throw new EmptyStringException('Строка пустая');
        }

        $string = trim($string, ' \t\n\r');

        $array = str_split($string);
        foreach ($array as $char)
        {

            $this->validateChar($char);

            if ($char == "(") {
                $this->pool++;
            }

            if ($char == ")") {
                $this->pool--;
            }
        }

        return $this->pool == 0;
    }

    private function validateChar(string $char) {

        if ($char != "(" && $char != ")")
        {
            throw new InvalidArgumentException('Строка содержит недостимые символы');
        }
    }
}