# Библиотека принимает строку, проверяет все ли открытие скобки корректно закрыты.
# Строка может включать символы “(“, “)”, “ ” (пробел), “\n” (перенос строки), “\t” (символ
#  табуляции), “\r” (перенос каретки). Если же строка содержит что-то кроме
#  перечисленных символов, то выбрасывается исключение InvalidArgumentException.